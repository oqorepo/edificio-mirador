$(window).load(function() {
	$('#preloader').fadeOut('slow');
	$('body').css({'overflow':'visible'});
});


var opts = {"container": "body", "trigger": "manual", "html": true};
if ($('#small').is(':visible'))
	$('#small').tooltip(opts).tooltip("show");
if ($('#medium').is(':visible'))
	$('#medium').tooltip(opts).tooltip("show");


	(function($) {
	  "use strict";
	  $(function() {
		$('.btn-nav[href*="#"]:not([href="#"])').click(function() {
		  if (
			location.pathname.replace(/^\//, "") ==
			  this.pathname.replace(/^\//, "") &&
			location.hostname == this.hostname
		  ) {
			var target = $(this.hash);
			target = target.length
			  ? target
			  : $("[name=" + this.hash.slice(1) + "]");
			if (target.length) {
			  $("html, body").animate(
				{
				  scrollTop: target.offset().top -40
				},
				1000
			  );
			  return false;
			}
		  }
		});
	  });
	})(jQuery);

/* open modals */
$(document).ready(function(){

	var altura = $('.menu').offset().top;
		  
	$(window).on('scroll', function(){
		if ( $(window).scrollTop() > altura ){
			$('.menu').addClass('menu-fixed');
		} else {
			$('.menu').removeClass('menu-fixed');
		}
	});

	// Script to Activate the Carousel
	$(".carousel").carousel({
		interval: 5000 //changes the speed
	});

	// compartir
	$("#compartir1, #compartir2, #compartir3, #compartir4, #compartir5, #compartir6").jsSocials({
		showLabel: false,
		showCount: false,
		shareIn: "popup",
		shares: ["whatsapp", "facebook", "twitter"]
	});

	// This event fires immediately when the slide instance method is invoked.
	$('#myCarousel').on('slide.bs.carousel', function (e) {
		var id = $('.item.active').data('slide-number');
		// Added a statement to make sure the carousel loops correct
		if(e.direction == 'right'){
			id = parseInt(id) - 1;  
			if(id == -1) id = 7;
		} else{
			id = parseInt(id) + 1;
			if(id == $('[id^=carousel-thumb-]').length) id = 0;
		}
		$('[id^=carousel-thumb-]').removeClass('selected');
		$('[id=carousel-thumb-' + id + ']').addClass('selected');
	});

	// Thumb control
	$('[id^=carousel-thumb-]').click( function(){
	  var id_selector = $(this).attr("id");
	  var id = id_selector.substr(id_selector.length -1);
	  id = parseInt(id);
	  $('#myCarousel').carousel(id);
	  $('[id^=carousel-thumb-]').removeClass('selected');
	  $(this).addClass('selected');
	});



	$(".btn-formulario-a").click(function(){
		$(".form-planta-iframe-a").toggle();
		$('#caja_formulario').load('https://frm.oqodigital.net/formularios/landing-edificio-mirador/?planta=Depto-A');
	});
	$(".btn-cerrar-a").click(function(){ $(".form-planta-iframe-a").toggle(); });

	$(".btn-formulario-b").click(function(){ $(".form-planta-iframe-b").toggle(); });
	$(".btn-cerrar-b").click(function(){ $(".form-planta-iframe-b").toggle(); });

	$(".btn-formulario-c").click(function(){ $(".form-planta-iframe-c").toggle(); });
	$(".btn-cerrar-c").click(function(){ $(".form-planta-iframe-c").toggle(); });

	$(".btn-formulario-d").click(function(){ $(".form-planta-iframe-d").toggle(); });
	$(".btn-cerrar-d").click(function(){ $(".form-planta-iframe-d").toggle(); });

	$(".btn-formulario-d2").click(function(){ $(".form-planta-iframe-d2").toggle(); });
	$(".btn-cerrar-d2").click(function(){ $(".form-planta-iframe-d2").toggle(); });

	$(".btn-formulario-e").click(function(){
		$(".form-planta-iframe-e").toggle();
	});
	$(".btn-cerrar-e").click(function(){ $(".form-planta-iframe-e").toggle(); });


});